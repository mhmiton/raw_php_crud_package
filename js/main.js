$(document).ready(function() {
    $('#dataTable').DataTable();

    $('.modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    $('.get_form').click(function(){
        var id      = $(this).data('id');
        var type    = $(this).data('type');
        var url     = $(this).data('url');

        $.ajax({
            type:'POST',
            url:url,
            data:{'id':id, 'type':type},
            dataType:'html',
            success:function(res)
            {
                $('#load_form').html(res);
                modal_static();
                $('#formModal').modal('show');
            }
        });
    });

    $('.delete').click(function(){
        var id      = $(this).data('id');
        var url     = $(this).data('url');
        
        swal({title:'Are You Sure Delete This Data', icon:'warning', buttons:true, dangerMode:true})
        .then((willDelete) => {
          if(willDelete)
          {
              location.replace(url+'?id='+id);
          }
        });
    });
});

function modal_static()
{
    $('.modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });
}

//Allow Only Nmber Digits /0-9/
function number(e)
{
    var k = e.which;
    if($.inArray(k, [0, 8, 9, 27, 13, 190]) == -1 && (k < 48 || k > 57) && (k < 2534 || k > 2543)) { return false; }
}
