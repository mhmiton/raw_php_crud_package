<?php
    include('../My_function.php');

    $id     = $_POST['id'];
    $type   = $_POST['type'];

    if($type == 'edit') {
        $data = read('one', 'location', "id = '$id'", '');
    } else {
        $data = null;
    }
?>

<form name="form" id="form" action="save.php" method="post" enctype="multipart/form-data">
    <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <h5 class="modal-title"><?php echo ($data) ? 'Edit' : 'Add' ?> Location</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset();">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="row modal-body">
            <div class="col-md-4 form-group">
                <label>City</label>
                <input class="form-control" type="text" name="city" id="city" required value="<?php echo ($data) ? $data->city : '' ?>">
            </div>

            <div class="col-md-4 form-group">
                <label>State</label>
                <input class="form-control" type="text" name="state" id="state" required value="<?php echo ($data) ? $data->state : '' ?>">
            </div>

            <div class="col-md-4 form-group">
                <label>Zip</label>
                <input class="form-control" type="text" name="zip" id="zip" required value="<?php echo ($data) ? $data->zip : '' ?>">
            </div>

            <div class="col-md-12 form-group">
                <label>Country</label>
                <input class="form-control" type="text" name="country" id="country" required value="<?php echo ($data) ? $data->country : '' ?>">
            </div>

            <div class="col-md-12 form-group">
                <label>Address</label>
                <input class="form-control" type="text" name="address" id="address" required value="<?php echo ($data) ? $data->address : '' ?>">
            </div>

            <div class="col-md-12 form-group">
                <label>Image</label>
                <span class="pull-right text-success"><?php echo ($data) ? $data->image : '' ?></span>
                <input class="form-control" type="file" name="image" id="image" <?php echo ($data) ? '' : 'required'; ?>>
            </div>

            <div class="col-md-12 form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" id="description" required rows="5"><?php echo ($data) ? $data->description : '' ?></textarea>
            </div>

          </div>

          <div class="modal-footer">
            <span id="hide_input">
                <input type="hidden" name="id" id="id" value="<?php echo ($data) ? $data->id : '' ?>">
            </span>

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-info" type="submit" id="submit"><?php echo ($data) ? 'Update' : 'Save' ?></button>
          </div>

        </div>
      </div>
    </div>
</form>
