<?php
    include('../My_function.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

    <script src="../js/main.js" type="text/javascript" language="javascript"></script>
    <script src="../js/notify.min.js" type="text/javascript" language="javascript"></script>
    <script src="../js/sweetalert.min.js" type="text/javascript" language="javascript"></script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.js" type="text/javascript"></script> -->

    <title>Raw PHP Crud Package (Custom Function)  - Very Usefull</title>
  </head>
  <body>
    <div class="jumbotron text-center">
        <h2 class="text-success">Raw PHP Crud Package (Custom Function) - Very Usefull</h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-success get_form" data-id="" data-type="save" data-url="form.php"><i class="fa fa-plus"></i> Add New Chart</button>
                <br><br><br>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table id="dataTable" class="table table-striped table-borderless table-dark table-responsive-md">
                    <thead class="bg-success">
                        <tr>
                            <td scope="row" align="left">SL</td>
                            <td scope="row" align="left">City</td>
                            <td scope="row" align="left">State</td>
                            <td scope="row" align="center">Zip</td>
                            <td scope="row" align="center">Country</td>
                            <td scope="row" align="center">Image</td>
                            <td scope="row" align="right">Action</td>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            $location = read('all', 'location', '', 'id DESC', '');
                            foreach ($location as $key => $v) {
                        ?>
                            <tr>
                                <td align="left"><?php echo $key+1 ?></td>
                                <td align="left"><?php echo $v->city; ?></td>
                                <td align="left"><?php echo $v->zip; ?></td>
                                <td align="center"><?php echo $v->city; ?></td>
                                <td align="center"><?php echo $v->country; ?></td>
                                <td align="center"><img width="150" height="80" src="<?php echo '../upload/'.$v->image; ?>"></td>

                                <td align="right">
                                    <i class="btn btn-info fa fa-edit get_form" data-id="<?php echo $v->id; ?>" data-type="edit" data-url="form.php"></i>
                                    <i class="btn btn-danger fa fa-trash delete" data-id="<?php echo $v->id; ?>" data-url="delete.php"></i>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="load_form"></div>    

    <br><br><br>
  </body>
</html>
