<?php
    include('../My_function.php');

    $data['city']           = $_POST['city'];
    $data['state']          = $_POST['state'];
    $data['zip']            = $_POST['zip'];
    $data['country']        = $_POST['country'];
    $data['address']        = $_POST['address'];
    $data['description']    = $_POST['description'];
    $id                     = $_POST['id'];

    $field = ['image'];
    foreach($field as $key => $img)
    {
        if($_FILES[$img]['name'])
        {
            $path           = '../upload/';
            $name           = time()+($key+1);
            $size           = ['300', '590'];
            $convert        = 'png';
            $upload         = image($img, $name, $path, $size, $convert);
            $data[$img]     = $upload;

            if($id) { delete_img('location', "id = '$id'", $path, $img); }
         } 
    }

    if(!$id)
    {        
        $save = save('location', $data);

        if($save == 'true')
        {
            echo '
                <script type="text/javascript">
                    alert("Data Save Successfully...");
                    location.replace("list.php");
                </script>
            ';
        } else {
            echo $save;
        }
    } else {
        $update = update('location', $data, "id = '$id'");

        if($update == 'true')
        {
            echo '
                <script type="text/javascript">
                    alert("Data Update Successfully...");
                    location.replace("list.php");
                </script>
            ';
        } else {
            echo $update;
        }
    }
?>